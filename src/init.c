#include <stdint.h>

/* Addresses from the linker script */
extern uint32_t _endsram;
extern uint32_t _endtext;
extern uint32_t _startdata;
extern uint32_t _enddata;
extern uint32_t _startbss;
extern uint32_t _endbss;

/* Flash configuration field */
uint8_t flashconfig[] __attribute__ ((section(".flashconfigs"))) = {
	0xFF,
	0xFF,
	0xFF,
	0xFF,
	0xFF,
	0xFF,
	0xFF,
	0xFF,
	0xFF,
	0xFF,
	0xFF,
	0xFF,
	0xFE,
	0xFF,
	0xFF,
	0xFF,
};

extern void main(void);
void resetexit(void);

/* Interrupt Service Routines */
void isrrfu(void) __attribute__ ((interrupt("IRQ")));
void isrnmi(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrhardfault(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrsvcall(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrpendsrvreq(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrsystick(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrdma0(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrdma1(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrdma2(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrdma3(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrftfa(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrpmc(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrllwu(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isri2c0(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isri2c1(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrspi0(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrspi1(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isruart0(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isruart1(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isruart2(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isradc0(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrcmp0(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrtpm0(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrtpm1(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrtpm2(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrrtcalarm(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrrtcseconds(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrpit(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrmcg(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrlptmr0(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrporta(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));
void isrportd(void) __attribute__ ((weak, interrupt("IRQ"), alias("isrrfu")));

/* Initial SP, PC, and interrupt vector table for MKL14Z4 */
void *vector_table[] __attribute__ ((section(".vectors"))) = {
	&_endsram, /* Initial SP */
	resetexit, /* Initial PC */
	isrnmi,
	isrhardfault,
	isrrfu,
	isrrfu,
	isrrfu,
	isrrfu,
	isrrfu,
	isrrfu,
	isrrfu,
	isrsvcall,
	isrrfu,
	isrrfu,
	isrpendsrvreq,
	isrsystick,
	isrdma0,
	isrdma1,
	isrdma2,
	isrdma3,
	isrrfu,
	isrftfa,
	isrpmc,
	isrllwu,
	isri2c0,
	isri2c1,
	isrspi0,
	isrspi1,
	isruart0,
	isruart1,
	isruart2,
	isradc0,
	isrcmp0,
	isrtpm0,
	isrtpm1,
	isrtpm2,
	isrrtcalarm,
	isrrtcseconds,
	isrpit,
	isrrfu,
	isrrfu,
	isrrfu,
	isrrfu,
	isrmcg,
	isrlptmr0,
	isrrfu,
	isrporta,
	isrportd,
};

void resetexit(void)
{
	uint8_t *src, *dst;

	/* Copy data section from flash to RAM */
	src = (uint8_t *)&_endtext;
	dst = (uint8_t *)&_startdata;
	while (dst < (uint8_t *)&_enddata)
		*dst++ = *src++;

	/* Clear the bss section */
	dst = (uint8_t *)&_startbss;
	while (dst < (uint8_t *)&_endbss)
		*dst++ = 0;

	main();
}

void isrrfu(void)
{
	/* FIXME: Fail fashionably */
	while (1);
}
