## Tools and Software Needed

* A PC with Linux
* [gcc-arm-none-eabi](https://launchpad.net/gcc-arm-embedded)
* GNU Make
* [Segger J-Link](http://www.segger.com/debug-probes.html)
* [Segger J-Link Software](http://www.segger.com/jlink-software.html)

## Build Instructions

    make

Output will be in the `build/` directory by default.

## Debugging Instructions

Power the board.  Connect the J-Link to the board.  Connect J-Link to USB.

Launch the Segger J-Link GDB server and use SWD interface:

    JLinkGDBServer -if SWD

If you have issues connecting, you can usually overcome them by specifying the
device name when launching the GDB server, such as:

    JLinkGDBServer -if SWD -device MKL14Z32xxx4

Then launch GDB from within the directory (and use the TUI):

    arm-none-eabi-gdb -tui

If you recompile, reload the symbols into GDB and then load the firmware onto
the board:

    file build/out.elf
    load build/out.elf
