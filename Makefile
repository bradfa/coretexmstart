# Outputs an elf, hex, and map file.

# To use a different cross compiler, set CROSS_COMPILE with trailing '-'.
# Set your output file name base in TARGET.
# Set your linker command file name in LINKERCMD.
# Set your CPU name in MCPU.

TARGET = $(BUILDDIR)/out
LINKERCMD = src/mkl14z32.ld
MCPU = cortex-m0plus

CROSS_COMPILE ?= arm-none-eabi-
CC = $(CROSS_COMPILE)gcc
OBJCOPY = $(CROSS_COMPILE)objcopy
CFLAGS = -ggdb -Wall -ffreestanding -Werror -Wextra -mthumb -mcpu=$(MCPU) -Iinclude/
LDFLAGS = -T $(LINKERCMD) -nostartfiles -Xlinker -Map=$(TARGETMAP)
SOURCES = $(wildcard src/*.c)
OBJECTS = $(patsubst src/%,$(BUILDDIR)/%,$(patsubst %.c,%.o,$(SOURCES)))
DFILES = $(OBJECTS:.o=.d)
BUILDDIR := build

TARGETELF = $(TARGET).elf
TARGETMAP = $(TARGET).map
TARGETHEX = $(TARGET).hex

.PHONY: all
all: $(TARGETELF) $(TARGETHEX)

$(TARGETELF): $(OBJECTS) $(LINKERCMD) | $(BUILDDIR)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJECTS) -o $@

$(TARGETHEX): $(TARGETELF) | $(BUILDDIR)
	$(OBJCOPY) -O ihex $^ $@

$(BUILDDIR)/%.o: src/%.c | $(BUILDDIR)
	$(CC) -c $(CFLAGS) $< -o $@

# Each .c file depends on #included headers
include $(DFILES)
$(BUILDDIR)/%.d: src/%.c | $(BUILDDIR)
	@set -e; rm -f $@; \
		$(CC) -MM $(CFLAGS) $< > $@.$$$$; \
		sed 's,\($*\)\.o[ :]*,$(BUILDDIR)/\1.o $@ : ,g' < $@.$$$$ > $@; \
		rm -f $@.$$$$

$(BUILDDIR):
	mkdir $(BUILDDIR)

.PHONY: clean
clean:
	rm -f $(TARGETELF) $(OBJECTS) $(DFILES) $(TARGETMAP) $(TARGETHEX)
